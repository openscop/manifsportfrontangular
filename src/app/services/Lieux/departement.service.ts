import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {error} from 'util';

@Injectable()

export class DepartementService {

  apiDepartement: string;

  departement: any;
  commune: any;

  constructor(private http: HttpClient) {

    this.apiDepartement = 'http://localhost:8000/api/Departements/';

    //departement
    this.departement = function (id) {
      if (id < 110) {
        return id + '/';
      }
    };
    //commune
    this.commune = function (id) {
      if (id <= 110) {
        return id + '/communes/';
      } else {
        return error();
      }
    };
  }

  //functions for observate data of object results in api

  /////////////////////////////////////////////////////
  getDepartementAin() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(2)).pipe(map(data => data));
  }
  getCommunesAin() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(2));
  }
//////////////////////////////////////////////////////

  getDepartementAisne() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(3)).pipe(map(data => data));
  }
  getCommunesAisne() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(3));
  }
///////////////////////////////////////////////////////

  getDepartementAllier() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(4)).pipe(map(data => data));
  }
  getCommunesAllier() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(4));
  }
///////////////////////////////////////////////////////

  getDepartementAlpeHauteProvence() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(5)).pipe(map(data => data));
  }
  getCommunesAlpeHauteProvence() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(5));
  }
///////////////////////////////////////////////////////

  getDepartementHautesAlpes() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(6)).pipe(map(data => data));
  }
  getCommunesHautesAlpes() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(6));
  }
  ////////////////////////////////////////////////////////////

  getDepartementAlpesMaritimes() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(7)).pipe(map(data => data));
  }
  getCommunesAlpesMaritimes() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(7));
  }
  //////////////////////////////////////////////////////////////

  getDepartementArdeche() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(8)).pipe(map(data => data));
  }
  getCommunesArdeche() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(8));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementArdennes() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(9)).pipe(map(data => data));
  }
  getCommunesArdennes() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(9));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementAriege() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(10)).pipe(map(data => data));
  }
  getCommunesAriege() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(10));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementAube() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(11)).pipe(map(data => data));
  }
  getCommunesAube() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(11));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementAude() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(12)).pipe(map(data => data));
  }
  getCommunesAude() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(12));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementAveyron() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(13)).pipe(map(data => data));
  }
  getCommunesAveyron() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(13));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementBouchesduRhone() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(14)).pipe(map(data => data));
  }
  getCommunesBouchesduRhone() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(14));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCalvados() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(15)).pipe(map(data => data));
  }
  getCommunesCalvados() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(15));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCantal() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(16)).pipe(map(data => data));
  }
  getCommunesCantal() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(16));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCharente() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(17)).pipe(map(data => data));
  }
  getCommunesCharente() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(17));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCharenteMaritime() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(18)).pipe(map(data => data));
  }
  getCommunesCharenteMaritime() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(18));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCher() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(19)).pipe(map(data => data));
  }
  getCommunesCher() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(19));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCorreze() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(20)).pipe(map(data => data));
  }
  getCommunesCorreze() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(20));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCorseduSud() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(21)).pipe(map(data => data));
  }
  getCommunesCorseduSud() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(21));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementHauteCorse() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(22)).pipe(map(data => data));
  }
  getCommunesHauteCorse() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(22));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCote_d_or() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(23)).pipe(map(data => data));
  }
  getCommunesCote_d_or() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(23));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCotes_d_Armor() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(24)).pipe(map(data => data));
  }
  getCommunesCotes_d_Armor() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(24));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementCreuse() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(25)).pipe(map(data => data));
  }
  getCommunesCreuse() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(25));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementDordogne() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(26)).pipe(map(data => data));
  }
  getCommunesDordogne() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(26));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementDoubs() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(27)).pipe(map(data => data));
  }
  getCommunesDoubs() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(27));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementDrome() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(28)).pipe(map(data => data));
  }
  getCommunesDrome() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(28));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementEure() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(29)).pipe(map(data => data));
  }
  getCommunesEure() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(29));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementEureEtLoir() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(30)).pipe(map(data => data));
  }
  getCommunesEureEtLoir() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(30));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementFinistere() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(31)).pipe(map(data => data));
  }
  getCommunesFinistere() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(31));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementGard() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(32)).pipe(map(data => data));
  }
  getCommunesGard() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(32));
  }
  /////////////////////////////////////////////////////////////////

  getDepartementHauteGaronne() {
    return this.http.get<any[]>(this.apiDepartement + this.departement(33)).pipe(map(data => data));
  }
  getCommunesHauteGaronne() {
    return this.http.get<any[]>(this.apiDepartement + this.commune(33));
  }
  /////////////////////////////////////////////////////////////////

}
