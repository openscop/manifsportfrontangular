import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DisciplinesService {

  apiDiscipline: string;

  constructor(private http: HttpClient) {
    this.apiDiscipline = 'http://localhost:8000/api/Disciplines/';
  }

  getDiscipline1() {
    return this.http.get<any[]>(this.apiDiscipline + '1/').pipe(map(data => data));
  }
  getDiscipline2() {
    return this.http.get<any[]>(this.apiDiscipline + '2/').pipe(map(data => data));
  }
  getDiscipline3() {
    return this.http.get<any[]>(this.apiDiscipline + '3/').pipe(map(data => data));
  }
  getDiscipline4() {
    return this.http.get<any[]>(this.apiDiscipline + '4/').pipe(map(data => data));
  }
  getDiscipline5() {
    return this.http.get<any[]>(this.apiDiscipline + '5/').pipe(map(data => data));
  }
  getDiscipline6() {
    return this.http.get<any[]>(this.apiDiscipline + '6/').pipe(map(data => data));
  }
  getDiscipline7() {
    return this.http.get<any[]>(this.apiDiscipline + '7/').pipe(map(data => data));
  }
  getDiscipline8() {
    return this.http.get<any[]>(this.apiDiscipline + '8/').pipe(map(data => data));
  }
  getDiscipline9() {
    return this.http.get<any[]>(this.apiDiscipline + '9/').pipe(map(data => data));
  }
  getDiscipline10() {
    return this.http.get<any[]>(this.apiDiscipline + '10/').pipe(map(data => data));
  }
  getDiscipline11() {
    return this.http.get<any[]>(this.apiDiscipline + '11/').pipe(map(data => data));
  }
  getDiscipline12() {
    return this.http.get<any[]>(this.apiDiscipline + '12/').pipe(map(data => data));
  }
  getDiscipline13() {
    return this.http.get<any[]>(this.apiDiscipline + '13/').pipe(map(data => data));
  }
  getDiscipline14() {
    return this.http.get<any[]>(this.apiDiscipline + '14/').pipe(map(data => data));
  }
  getDiscipline15() {
    return this.http.get<any[]>(this.apiDiscipline + '15/').pipe(map(data => data));
  }
  getDiscipline16() {
    return this.http.get<any[]>(this.apiDiscipline + '16/').pipe(map(data => data));
  }
  getDiscipline17() {
    return this.http.get<any[]>(this.apiDiscipline + '17/').pipe(map(data => data));
  }
  getDiscipline18() {
    return this.http.get<any[]>(this.apiDiscipline + '18/').pipe(map(data => data));
  }
  getDiscipline19() {
    return this.http.get<any[]>(this.apiDiscipline + '19/').pipe(map(data => data));
  }
  getDiscipline20() {
    return this.http.get<any[]>(this.apiDiscipline + '20/').pipe(map(data => data));
  }
  getDiscipline21() {
    return this.http.get<any[]>(this.apiDiscipline + '21/').pipe(map(data => data));
  }
  getDiscipline22() {
    return this.http.get<any[]>(this.apiDiscipline + '22/').pipe(map(data => data));
  }
  getDiscipline23() {
    return this.http.get<any[]>(this.apiDiscipline + '23/').pipe(map(data => data));
  }

}
