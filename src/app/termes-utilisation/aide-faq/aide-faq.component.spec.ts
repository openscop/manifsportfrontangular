import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AideFaqComponent } from './aide-faq.component';

describe('AideFaqComponent', () => {
  let component: AideFaqComponent;
  let fixture: ComponentFixture<AideFaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AideFaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AideFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
