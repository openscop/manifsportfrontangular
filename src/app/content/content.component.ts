import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  authenticate: boolean;

  constructor() { }

  ngOnInit() {
    if (localStorage.getItem('token') != null) {
    this.authenticate = true;
  }
  }

}
