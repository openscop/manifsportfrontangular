//module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AlertModule } from 'ngx-bootstrap';
import { CarouselModule } from 'ngx-bootstrap';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {OAuthModule} from 'angular-oauth2-oidc';


//module angular Material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatInputModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatMenuModule} from '@angular/material/menu';






//service
import {AuthGuard} from './services/Auth/auth-guard.service';
//import { LoginService} from './services/login.service';
import {DepartementService} from './services/Lieux/departement.service';






//Routes & component
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormEventsComponent } from './form-events/form-events.component';
import { Dept42Component } from './create-route/dept42/dept42.component';
import { Dept78Component } from './create-route/dept78/dept78.component';
import { Dept972Component } from './create-route/dept972/dept972.component';
import { AutherDepComponent } from './create-route/auther-dep/auther-dep.component';
import { EventComponent } from './form-events/event/event.component';
import { MotorizedEventComponent } from './form-events/motorized-event/motorized-event.component';
import { RoadMotorizedEventComponent } from './form-events/road-motorized-event/road-motorized-event.component';
import { FormMotorizedComponent } from './form-events/form-motorized/form-motorized.component';
import { MotorizedVehicleNoneComponent } from './form-events/motorized-vehicle-none/motorized-vehicle-none.component';
import { GroupCirculationComponent } from './form-events/group-circulation/group-circulation.component';
import { FormCompetitionComponent } from './form-events/form-competition/form-competition.component';
import { FormMotorizedRaceComponent } from './form-events/form-motorized-race/form-motorized-race.component';
import { FormVehicleMotorizedComponent } from './form-events/form-vehicle-motorized/form-vehicle-motorized.component';
import { DashboardInstructeurComponent } from './dashboard-instructeur/dashboard-instructeur.component';
import { ArchivesComponent } from './dashboard-instructeur/archives/archives.component';
import { AboutComponent } from './termes-utilisation/about/about.component';
import { CguComponent } from './termes-utilisation/cgu/cgu.component';
import { MentionLegaleComponent } from './termes-utilisation/mention-legale/mention-legale.component';
import { FormNmComponent } from './form-events/form-nm/form-nm.component';
import { AideComponent } from './termes-utilisation/aide/aide.component';
import { FaqComponent } from './termes-utilisation/faq/faq.component';
import { AideFaqComponent } from './termes-utilisation/aide-faq/aide-faq.component';
import { CalendarComponent } from './calendar/calendar.component';




const appRoutes: Routes = [
  //add Guard service in route -->  after path wright > canActivate: [AuthGuard],
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'sign-in', component: SigninComponent},
  {path: 'dashboard', canActivate : [AuthGuard], component: DashboardComponent},
  {path: 'events/forms_event', canActivate : [AuthGuard], component: FormEventsComponent},
  {path: 'authorizations/dashboard', canActivate : [AuthGuard], component: DashboardInstructeurComponent},
  {path: 'authorizations/archives', canActivate : [AuthGuard], component: ArchivesComponent},
  {path: 'calendrier', canActivate : [AuthGuard], component: CalendarComponent},

  {path: 'create-route/dept42', canActivate : [AuthGuard], component: Dept42Component},
  {path: 'create-route/dept78', canActivate : [AuthGuard], component: Dept78Component},
  {path: 'create-route/dept972', canActivate : [AuthGuard], component: Dept972Component},
  {path: 'auther-dep', canActivate : [AuthGuard], component: AutherDepComponent},
  {path: 'dept/choose/event', canActivate : [AuthGuard], component: EventComponent},
  {path: 'dept/choose/motorized', canActivate : [AuthGuard], component: MotorizedEventComponent},
  {path: 'dept/choose/roadmotorizedevent', canActivate : [AuthGuard], component: RoadMotorizedEventComponent},
  {path: 'dept/add/motorizedconcentration', canActivate : [AuthGuard], component: FormMotorizedComponent},
  {path: 'dept/choose/motorized-vehicle-none', canActivate : [AuthGuard], component: MotorizedVehicleNoneComponent},
  {path: 'dept/choose/group-circulation', canActivate : [AuthGuard], component: GroupCirculationComponent},
  {path: 'dept/add/vehicle-motorized', canActivate : [AuthGuard], component: FormVehicleMotorizedComponent},
  {path: 'dept/add/motorizedRace', canActivate : [AuthGuard], component: FormMotorizedRaceComponent},
  {path: 'dept/add/competition', canActivate : [AuthGuard], component: FormCompetitionComponent},
  {path: 'dept/add/declarationNM', canActivate : [AuthGuard], component: FormNmComponent},



  {path: 'about', component: AboutComponent},
  {path: 'aide', component: AideComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'mentions-legales', component: MentionLegaleComponent},
  {path: 'CGU', component: CguComponent},
  {path: 'aide_faq', component: AideFaqComponent},

  {path: '', component: HomeComponent},
  {path: 'not-found', component: NotFoundComponent},
  {path: '**', redirectTo: 'not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SliderComponent,
    ContentComponent,
    FooterComponent,
    LoginComponent,
    HomeComponent,
    SigninComponent,
    NotFoundComponent,
    DashboardComponent,
    FormEventsComponent,
    Dept42Component,
    Dept78Component,
    Dept972Component,
    AutherDepComponent,
    EventComponent,
    MotorizedEventComponent,
    RoadMotorizedEventComponent,
    FormMotorizedComponent,
    MotorizedVehicleNoneComponent,
    GroupCirculationComponent,
    FormCompetitionComponent,
    FormMotorizedRaceComponent,
    FormVehicleMotorizedComponent,
    DashboardInstructeurComponent,
    ArchivesComponent,
    AboutComponent,
    CguComponent,
    MentionLegaleComponent,
    FormNmComponent,
    AideComponent,
    FaqComponent,
    AideFaqComponent,
    CalendarComponent,

  ],
  imports: [
    CarouselModule.forRoot(),
    AlertModule.forRoot(),
    BrowserModule,
    HttpClientModule,
    OAuthModule.forRoot(),
    AngularFontAwesomeModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    MatInputModule,
    MatMenuModule
  ],
  providers: [
    AuthGuard,
    DepartementService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
