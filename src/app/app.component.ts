import { Component } from '@angular/core';
import {JwksValidationHandler, OAuthService} from 'angular-oauth2-oidc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private oauthService: OAuthService) {
    this.oauthService.clientId = 'eSACFTH8dpRpCtmZCFo55CIyJm6jk4GRmkWqpEIG';
    this.oauthService.scope = 'read write groups';
    this.oauthService.redirectUri = window.location.origin;
    this.oauthService.tokenEndpoint = 'http://localhost:8000/o/token/';
    this.oauthService.requireHttps = false;
    this.oauthService.oidc = false;
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();

  }
}
