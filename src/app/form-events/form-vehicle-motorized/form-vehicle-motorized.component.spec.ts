import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormVehicleMotorizedComponent } from './form-vehicle-motorized.component';

describe('FormVehicleMotorizedComponent', () => {
  let component: FormVehicleMotorizedComponent;
  let fixture: ComponentFixture<FormVehicleMotorizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormVehicleMotorizedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormVehicleMotorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
