import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCirculationComponent } from './group-circulation.component';

describe('GroupCirculationComponent', () => {
  let component: GroupCirculationComponent;
  let fixture: ComponentFixture<GroupCirculationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCirculationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCirculationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
