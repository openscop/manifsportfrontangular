import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';



@Component({
  selector: 'app-form-motorized',
  templateUrl: './form-motorized.component.html',
  styleUrls: ['./form-motorized.component.css']
})
export class FormMotorizedComponent implements OnInit {

  //multiselect
  toppings = new FormControl();
  departementTraverse = ['apiDep', 'apidep'];
  communeTraverse = ['apiDep', 'apidep'];

  //autocomplete
  myControl: FormControl = new FormControl();
  options = ['apiSport1', 'apiSport2', 'apiSport3'];
  filteredOptions: Observable<string[]>;

  constructor() {}

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(val => this.filter(val))
    );
  }

  filter(val: string): string[] {
    return this.options.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }
}



