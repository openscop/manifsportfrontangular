import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMotorizedComponent } from './form-motorized.component';

describe('FormMotorizedComponent', () => {
  let component: FormMotorizedComponent;
  let fixture: ComponentFixture<FormMotorizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMotorizedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMotorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
