import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MotorizedVehicleNoneComponent } from './motorized-vehicle-none.component';

describe('MotorizedVehicleNoneComponent', () => {
  let component: MotorizedVehicleNoneComponent;
  let fixture: ComponentFixture<MotorizedVehicleNoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotorizedVehicleNoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotorizedVehicleNoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
