import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMotorizedRaceComponent } from './form-motorized-race.component';

describe('FormMotorizedRaceComponent', () => {
  let component: FormMotorizedRaceComponent;
  let fixture: ComponentFixture<FormMotorizedRaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMotorizedRaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMotorizedRaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
