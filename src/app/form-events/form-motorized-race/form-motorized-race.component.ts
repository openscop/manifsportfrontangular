import { Component, OnInit } from '@angular/core';
import {FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {DisciplinesService} from '../../services/Sport/disciplines.service';
import {DepartementService} from '../../services/Lieux/departement.service';


@Component({
  selector: 'app-form-motorized-race',
  templateUrl: './form-motorized-race.component.html',
  styleUrls: ['./form-motorized-race.component.css']
})
export class FormMotorizedRaceComponent implements OnInit {

  discipline1$: any;
  discipline2$: any;
  discipline3$: any;
  discipline4$: any;
  discipline5$: any;
  discipline6$: any;
  discipline7$: any;
  discipline8$: any;
  discipline9$: any;
  discipline10$: any;
  discipline11$: any;
  discipline12$: any;
  discipline13$: any;
  discipline14$: any;
  discipline15$: any;
  discipline16$: any;
  discipline17$: any;
  discipline18$: any;
  discipline19$: any;
  discipline20$: any;
  discipline21$: any;
  discipline22$: any;
  discipline23$: any;


  //multiselect
  toppings = new FormControl();
  departementTraverse = ['ApiDep', 'apidep', 'apidep', 'apidep', 'apidep', 'apidep'];
  communeTraverse = ['ApiDep', 'apidep', 'apidep', 'apidep', 'apidep', 'apidep'];
  carto = ['carto1', 'carto2', 'carto3', 'carto4', 'carto5'];

  //for inputSearch
  myControl: FormControl = new FormControl();
  options = ['apiSport1', 'sport2', 'sport3', 'sport4'];
  filteredOptions: Observable<string[]>;

  constructor(private _svcDiscipline: DisciplinesService,
              private _svcDep: DepartementService) { }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(val => this.filter(val))
    );

    //call this function on init
    this.disciplines1();
    this.disciplines2();
    this.disciplines3();
    this.disciplines4();
    this.disciplines5();
    this.disciplines6();
    this.disciplines7();
    this.disciplines8();
    this.disciplines9();
    this.disciplines10();
    this.disciplines11();
    this.disciplines12();
    this.disciplines13();
    this.disciplines14();
    this.disciplines15();
    this.disciplines16();
    this.disciplines17();
    this.disciplines18();
    this.disciplines19();
    this.disciplines20();
    this.disciplines21();
    this.disciplines22();
    this.disciplines23();
  }


  filter(val: string): string[] {
    return this.options.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Data of discipline
  disciplines1() {
    this._svcDiscipline.getDiscipline1().subscribe(
      data => {
        this.discipline1$ = data;
        this.discipline1$ = Array.of(this.discipline1$);
      }
    );
  }
  disciplines2() {
    this._svcDiscipline.getDiscipline2().subscribe(
      data => {
        this.discipline2$ = data;
        this.discipline2$ = Array.of(this.discipline2$);
      }
    );
  }
  disciplines3() {
    this._svcDiscipline.getDiscipline3().subscribe(
      data => {
        this.discipline3$ = data;
        this.discipline3$ = Array.of(this.discipline3$);
      }
    );
  }
  disciplines4() {
    this._svcDiscipline.getDiscipline4().subscribe(
      data => {
        this.discipline4$ = data;
        this.discipline4$ = Array.of(this.discipline4$);
      }
    );
  }
  disciplines5() {
    this._svcDiscipline.getDiscipline5().subscribe(
      data => {
        this.discipline5$ = data;
        this.discipline5$ = Array.of(this.discipline5$);
      }
    );
  }
  disciplines6() {
    this._svcDiscipline.getDiscipline6().subscribe(
      data => {
        this.discipline6$ = data;
        this.discipline6$ = Array.of(this.discipline6$);
      }
    );
  }
  disciplines7() {
    this._svcDiscipline.getDiscipline7().subscribe(
      data => {
        this.discipline7$ = data;
        this.discipline7$ = Array.of(this.discipline7$);
      }
    );
  }
  disciplines8() {
    this._svcDiscipline.getDiscipline8().subscribe(
      data => {
        this.discipline8$ = data;
        this.discipline8$ = Array.of(this.discipline8$);
      }
    );
  }
  disciplines9() {
    this._svcDiscipline.getDiscipline9().subscribe(
      data => {
        this.discipline9$ = data;
        this.discipline9$ = Array.of(this.discipline9$);
      }
    );
  }
  disciplines10() {
    this._svcDiscipline.getDiscipline10().subscribe(
      data => {
        this.discipline10$ = data;
        this.discipline10$ = Array.of(this.discipline10$);
      }
    );
  }
  disciplines11() {
    this._svcDiscipline.getDiscipline11().subscribe(
      data => {
        this.discipline11$ = data;
        this.discipline11$ = Array.of(this.discipline11$);
      }
    );
  }
  disciplines12() {
    this._svcDiscipline.getDiscipline12().subscribe(
      data => {
        this.discipline12$ = data;
        this.discipline12$ = Array.of(this.discipline12$);
      }
    );
  }
  disciplines13() {
    this._svcDiscipline.getDiscipline13().subscribe(
      data => {
        this.discipline13$ = data;
        this.discipline13$ = Array.of(this.discipline13$);
      }
    );
  }
  disciplines14() {
    this._svcDiscipline.getDiscipline14().subscribe(
      data => {
        this.discipline14$ = data;
        this.discipline14$ = Array.of(this.discipline14$);
      }
    );
  }
  disciplines15() {
    this._svcDiscipline.getDiscipline15().subscribe(
      data => {
        this.discipline15$ = data;
        this.discipline15$ = Array.of(this.discipline15$);
      }
    );
  }
  disciplines16() {
    this._svcDiscipline.getDiscipline16().subscribe(
      data => {
        this.discipline16$ = data;
        this.discipline16$ = Array.of(this.discipline16$);
      }
    );
  }
  disciplines17() {
    this._svcDiscipline.getDiscipline17().subscribe(
      data => {
        this.discipline17$ = data;
        this.discipline17$ = Array.of(this.discipline17$);
      }
    );
  }
  disciplines18() {
    this._svcDiscipline.getDiscipline18().subscribe(
      data => {
        this.discipline18$ = data;
        this.discipline18$ = Array.of(this.discipline18$);
      }
    );
  }
  disciplines19() {
    this._svcDiscipline.getDiscipline19().subscribe(
      data => {
        this.discipline19$ = data;
        this.discipline19$ = Array.of(this.discipline19$);
      }
    );
  }
  disciplines20() {
    this._svcDiscipline.getDiscipline20().subscribe(
      data => {
        this.discipline20$ = data;
        this.discipline20$ = Array.of(this.discipline20$);
      }
    );
  }
  disciplines21() {
    this._svcDiscipline.getDiscipline21().subscribe(
      data => {
        this.discipline21$ = data;
        this.discipline21$ = Array.of(this.discipline21$);
      }
    );
  }
  disciplines22() {
    this._svcDiscipline.getDiscipline22().subscribe(
      data => {
        this.discipline22$ = data;
        this.discipline22$ = Array.of(this.discipline22$);
      }
    );
  }
  disciplines23() {
    this._svcDiscipline.getDiscipline23().subscribe(
      data => {
        this.discipline23$ = data;
        this.discipline23$ = Array.of(this.discipline23$);
      }
    );
  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
