import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MotorizedEventComponent } from './motorized-event.component';

describe('MotorizedEventComponent', () => {
  let component: MotorizedEventComponent;
  let fixture: ComponentFixture<MotorizedEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotorizedEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotorizedEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
