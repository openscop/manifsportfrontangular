import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNmComponent } from './form-nm.component';

describe('FormNmComponent', () => {
  let component: FormNmComponent;
  let fixture: ComponentFixture<FormNmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
