import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-form-nm',
  templateUrl: './form-nm.component.html',
  styleUrls: ['./form-nm.component.css']
})
export class FormNmComponent implements OnInit {
  //multi select
  toppings = new FormControl();
  dptTrav = ['apiDep', 'apiDep2', 'apiDep3', 'apiDep4',
            'apiDep5', 'apiDep6', 'apiDep7', 'apiDep8',
            'apiDep9', 'apiDep10'];
  communeTrav = ['apiDep', 'apiDep2', 'apiDep3', 'apiDep4',
                'apiDep5', 'apiDep6', 'apiDep7', 'apiDep8',
                'apiDep9', 'apiDep10'];
  carto = ['carto1', 'carto2', 'carto3', 'carto4', 'carto5', 'carto6', 'carto7'];


  //autocomplete
  myControl: FormControl = new FormControl();
  options = ['apiDiscipline', 'apiDiscipline2', 'apiDiscipline3'];
  filteredOptions: Observable<string[]>;

  constructor() {
  }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(val => this.filter(val))
    );
  }
  filter(val: string): string[] {
    return this.options.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }
}
