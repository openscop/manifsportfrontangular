import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadMotorizedEventComponent } from './road-motorized-event.component';

describe('RoadMotorizedEventComponent', () => {
  let component: RoadMotorizedEventComponent;
  let fixture: ComponentFixture<RoadMotorizedEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadMotorizedEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadMotorizedEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
