import { Component } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-form-competition',
  templateUrl: './form-competition.component.html',
  styleUrls: ['./form-competition.component.css']
})
export class FormCompetitionComponent{
  //multi select
  toppings = new FormControl();

  toppingList = ['ApiDep1', 'ApiDep2', 'ApiDep3', 'ApiDep4'];

  toppingCarto = ['carto1', 'carto2', 'carto3', 'carto4'];

}
