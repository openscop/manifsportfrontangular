import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {OAuthService} from 'angular-oauth2-oidc';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  authenticate: boolean;
  isCollapsed: boolean;

  constructor(private oauthService: OAuthService,
              private router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('token') != null) {
      this.authenticate = true;
    }
    this.isCollapsed = true;
  }

  logout() {

    localStorage.removeItem('token');
    this.router.navigate(['/login']);

    window.location.reload();
  }
}

