import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { OAuthService} from 'angular-oauth2-oidc';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  access_token: string;

  loginFailed: boolean;
  authenticate: boolean;

  constructor(private router: Router,
              private oauthService: OAuthService) {

  }

  ngOnInit() {

    if (localStorage.getItem('token') != null) {
      this.authenticate = true;
    }
  }

  loginWithPassword() {
    this.oauthService
      .fetchTokenUsingPasswordFlow(
        this.username,
        this.password
      )
      .then((response) => {

        console.log('successfully logged in');
        this.access_token = response['access_token'];
        localStorage.setItem('token', this.access_token);
        this.router.navigate(['/home']);
        this.loginFailed = false;

        window.location.reload();
      })
      .catch(err => {
        console.log('error logging in', err);
        this.loginFailed = true;
        this.authenticate = false;
      });
  }
}
