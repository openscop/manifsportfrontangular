import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dept78Component } from './dept78.component';

describe('Dept78Component', () => {
  let component: Dept78Component;
  let fixture: ComponentFixture<Dept78Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dept78Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dept78Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
