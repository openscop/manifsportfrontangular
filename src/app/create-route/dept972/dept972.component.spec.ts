import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dept972Component } from './dept972.component';

describe('Dept972Component', () => {
  let component: Dept972Component;
  let fixture: ComponentFixture<Dept972Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dept972Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dept972Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
