import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutherDepComponent } from './auther-dep.component';

describe('AutherDepComponent', () => {
  let component: AutherDepComponent;
  let fixture: ComponentFixture<AutherDepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutherDepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutherDepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
