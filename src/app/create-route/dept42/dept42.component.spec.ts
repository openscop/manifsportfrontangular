import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dept42Component } from './dept42.component';

describe('Dept42Component', () => {
  let component: Dept42Component;
  let fixture: ComponentFixture<Dept42Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dept42Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dept42Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
