import { Component, OnInit } from '@angular/core';
import {DepartementService} from '../services/Lieux/departement.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  selectedCommuneDepartement: string;

  departementOfAin: any;
  communesOfAin: any;

  departementOfAisne: any;
  communesOfAisne: any;

  departementOfAllier: any;
  communesOfAllier: any;

  departementOfAlpesHp: any;
  communesOfAlpesHp: any;

  departementOfHautesAlpes: any;
  communesOfHautesAlpes: any;

  departementOfAlpesMaritimes: any;
  communesOfAlpesMaritimes: any;


  departementOfArdeche: any;
  communesOfArdeche: any;

  departementOfArdennes: any;
  communesOfArdennes: any;

  departementOfAriege: any;
  communesOfAriege: any;

  departementOfAube: any;
  communesOfAube: any;

  departementOfAude: any;
  communesOfAude: any;

  departementOfAveyron: any;
  communesOfAveyron: any;

  departementOfBouchesDuRhone: any;
  communesOfBouchesDuRhone: any;

  departementOfCalvados: any;
  communesOfCalvados: any;

  departementOfCantal: any;
  communesOfCantal: any;

  departementOfCharente: any;
  communesOfCharente: any;

  departementOfCharenteMaritime: any;
  communesOfCharenteMaritime: any;

  departementOfCher: any;
  communesOfCher: any;

  departementOfCorreze: any;
  communesOfCorreze: any;

  departementOfCorseduSud: any;
  communesOfCorseduSud: any;

  departementOfHauteCorse: any;
  communesOfHauteCorse: any;

  departementOfCote_d_or: any;
  communesOfCote_d_or: any;

  departementOfCotes_d_Armor: any;
  communesOfCotes_d_Armor: any;

  departementOfCreuse: any;
  communesOfCreuse: any;

  departementOfDordogne: any;
  communesOfDordogne: any;

  departementOfDoubs: any;
  communesOfDoubs: any;

  departementOfDrome: any;
  communesOfDrome: any;

  departementOfEure: any;
  communesOfEure: any;

  departementOfEureEtLoir: any;
  communesOfEureEtLoir: any;

  departementOfFinistere: any;
  communesOfFinistere: any;

  departementOfGard: any;
  communesOfGard: any;

  departementOfHauteGaronne: any;
  communesOfHauteGaronne: any;


  constructor(private _svcDepartement: DepartementService) { }

  ngOnInit() {
    //charge function on init
    this.departementAin();
    this.departementAisne();
    this.departementAllier();
    this.departementAlpesHp();
    this.departementHautesAlpes();
    this.departementAlpesMaritimes();
    this.departementArdeche();
    this.departementArdennes();
    this.departementAriege();
    this.departementAube();
    this.departementAude();
    this.departementAveyron();
    this.departementBouchesDuRhone();
    this.departementCalvados();
    this.departementCantal();
    this.departementCharente();
    this.departementCharenteMaritime();
    this.departementCher();
    this.departementCorreze();
    this.departementCorseduSud();
    this.departementHauteCorse();
    this.departementCote_d_or();
    this.departementCotes_d_Armor();
    this.departementCreuse();
    this.departementDordogne();
    this.departementDoubs();
    this.departementDrome();
    this.departementEure();
    this.departementEureEtLoir();
    this.departementFinistere();
    this.departementGard();
    this.departementHauteGaronne();
  }


  //functions for cunsoming api Departement (api on four pages)
 //////////////////////////////////////////////////////////////////
  departementAin() {
    this._svcDepartement.getDepartementAin().subscribe(
      data => {
        this.departementOfAin = data;
        this.departementOfAin = Array.of(this.departementOfAin);
      }
    );
  }
  communesAin() {
    this.communesOfAin = this._svcDepartement.getCommunesAin();
  }
////////////////////////////////////////////////////////////////////

  departementAisne() {
    this._svcDepartement.getDepartementAisne().subscribe(
      data => {
        this.departementOfAisne = data;
        this.departementOfAisne = Array.of(this.departementOfAisne);
      }
    );
  }
  communesAisne() {
    this.communesOfAisne = this._svcDepartement.getCommunesAisne();
  }
///////////////////////////////////////////////////////////////////////////

  departementAllier() {
    this._svcDepartement.getDepartementAllier().subscribe(
      data => {
        this.departementOfAllier = data;
        this.departementOfAllier = Array.of(this.departementOfAllier);
      }
    );
  }
  communesAllier() {
    this.communesOfAllier = this._svcDepartement.getCommunesAllier();
  }
//////////////////////////////////////////////////////////////////////

  departementAlpesHp() {
    this._svcDepartement.getDepartementAlpeHauteProvence().subscribe(
      data => {
        this.departementOfAlpesHp = data;
        this.departementOfAlpesHp = Array.of(this.departementOfAlpesHp);
      }
    );
  }
  communesAlpesHp() {
    this.communesOfAlpesHp = this._svcDepartement.getCommunesAlpeHauteProvence();
  }
 /////////////////////////////////////////////////////////////////////////////////

  departementHautesAlpes() {
    this._svcDepartement.getDepartementHautesAlpes().subscribe(
      data => {
        this.departementOfHautesAlpes = data;
        this.departementOfHautesAlpes = Array.of(this.departementOfHautesAlpes);
      }
    );
  }
  communesHautesAlpes() {
    this.communesOfHautesAlpes = this._svcDepartement.getCommunesHautesAlpes();
  }
////////////////////////////////////////////////////////////////////////////////////

  departementAlpesMaritimes() {
    this._svcDepartement.getDepartementAlpesMaritimes().subscribe(
      data => {
        this.departementOfAlpesMaritimes = data;
        this.departementOfAlpesMaritimes = Array.of(this.departementOfAlpesMaritimes);
      }
    );
  }
  communesAlpesMaritimes() {
    this.communesOfAlpesMaritimes = this._svcDepartement.getCommunesAlpesMaritimes();
  }
  ///////////////////////////////////////////////////////////////////////////////////

  departementArdeche() {
    this._svcDepartement.getDepartementArdeche().subscribe(
      data => {
        this.departementOfArdeche = data;
        this.departementOfArdeche = Array.of(this.departementOfArdeche);
      }
    );
  }
  communesArdeche() {
    this.communesOfArdeche = this._svcDepartement.getCommunesArdeche();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementArdennes() {
    this._svcDepartement.getDepartementArdennes().subscribe(
      data => {
        this.departementOfArdennes = data;
        this.departementOfArdennes = Array.of(this.departementOfArdennes);
      }
    );
  }
  communesArdennes() {
    this.communesOfArdennes = this._svcDepartement.getCommunesArdennes();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementAriege() {
    this._svcDepartement.getDepartementAriege().subscribe(
      data => {
        this.departementOfAriege = data;
        this.departementOfAriege = Array.of(this.departementOfAriege);
      }
    );
  }
  communesAriege() {
    this.communesOfAriege = this._svcDepartement.getCommunesAriege();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementAube() {
    this._svcDepartement.getDepartementAube().subscribe(
      data => {
        this.departementOfAube = data;
        this.departementOfAube = Array.of(this.departementOfAube);
      }
    );
  }
  communesAube() {
    this.communesOfAube = this._svcDepartement.getCommunesAube();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementAude() {
    this._svcDepartement.getDepartementAude().subscribe(
      data => {
        this.departementOfAude = data;
        this.departementOfAude = Array.of(this.departementOfAude);
      }
    );
  }
  communesAude() {
    this.communesOfAude = this._svcDepartement.getCommunesAude();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementAveyron() {
    this._svcDepartement.getDepartementAveyron().subscribe(
      data => {
        this.departementOfAveyron = data;
        this.departementOfAveyron = Array.of(this.departementOfAveyron);
      }
    );
  }
  communesAveyron() {
    this.communesOfAveyron = this._svcDepartement.getCommunesAveyron();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementBouchesDuRhone() {
    this._svcDepartement.getDepartementBouchesduRhone().subscribe(
      data => {
        this.departementOfBouchesDuRhone = data;
        this.departementOfBouchesDuRhone = Array.of(this.departementOfBouchesDuRhone);
      }
    );
  }
  communesBouchesDuRhone() {
    this.communesOfBouchesDuRhone = this._svcDepartement.getCommunesBouchesduRhone();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCalvados() {
    this._svcDepartement.getDepartementCalvados().subscribe(
      data => {
        this.departementOfCalvados = data;
        this.departementOfCalvados = Array.of(this.departementOfCalvados);
      }
    );
  }
  communesCalvados() {
    this.communesOfCalvados = this._svcDepartement.getCommunesCalvados();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCantal() {
    this._svcDepartement.getDepartementCantal().subscribe(
      data => {
        this.departementOfCantal = data;
        this.departementOfCantal = Array.of(this.departementOfCantal);
      }
    );
  }
  communesCantal() {
    this.communesOfCantal = this._svcDepartement.getCommunesCantal();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCharente() {
    this._svcDepartement.getDepartementCharente().subscribe(
      data => {
        this.departementOfCharente = data;
        this.departementOfCharente = Array.of(this.departementOfCharente);
      }
    );
  }
  communesCharente() {
    this.communesOfCharente = this._svcDepartement.getCommunesCharente();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCharenteMaritime() {
    this._svcDepartement.getDepartementCharenteMaritime().subscribe(
      data => {
        this.departementOfCharenteMaritime = data;
        this.departementOfCharenteMaritime = Array.of(this.departementOfCharenteMaritime);
      }
    );
  }
  communesCharenteMaritime() {
    this.communesOfCharenteMaritime = this._svcDepartement.getCommunesCharenteMaritime();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCher() {
    this._svcDepartement.getDepartementCher().subscribe(
      data => {
        this.departementOfCher = data;
        this.departementOfCher = Array.of(this.departementOfCher);
      }
    );
  }
  communesCher() {
    this.communesOfCher = this._svcDepartement.getCommunesCher();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCorreze() {
    this._svcDepartement.getDepartementCorreze().subscribe(
      data => {
        this.departementOfCorreze = data;
        this.departementOfCorreze = Array.of(this.departementOfCorreze);
      }
    );
  }
  communesCorreze() {
    this.communesOfCorreze = this._svcDepartement.getCommunesCorreze();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCorseduSud() {
    this._svcDepartement.getDepartementCorseduSud().subscribe(
      data => {
        this.departementOfCorseduSud = data;
        this.departementOfCorseduSud = Array.of(this.departementOfCorseduSud);
      }
    );
  }
  communesCorseduSud() {
    this.communesOfCorseduSud = this._svcDepartement.getCommunesCorseduSud();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementHauteCorse() {
    this._svcDepartement.getDepartementHauteCorse().subscribe(
      data => {
        this.departementOfHauteCorse = data;
        this.departementOfHauteCorse = Array.of(this.departementOfHauteCorse);
      }
    );
  }
  communesHauteCorse() {
    this.communesOfHauteCorse = this._svcDepartement.getCommunesHauteCorse();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCote_d_or() {
    this._svcDepartement.getDepartementCote_d_or().subscribe(
      data => {
        this.departementOfCote_d_or = data;
        this.departementOfCote_d_or = Array.of(this.departementOfCote_d_or);
      }
    );
  }
  communesCote_d_or() {
    this.communesOfCote_d_or = this._svcDepartement.getCommunesCote_d_or();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCotes_d_Armor() {
    this._svcDepartement.getDepartementCotes_d_Armor().subscribe(
      data => {
        this.departementOfCotes_d_Armor = data;
        this.departementOfCotes_d_Armor = Array.of(this.departementOfCotes_d_Armor);
      }
    );
  }
  communesCotes_d_Armor() {
    this.communesOfCotes_d_Armor = this._svcDepartement.getCommunesCotes_d_Armor();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementCreuse() {
    this._svcDepartement.getDepartementCreuse().subscribe(
      data => {
        this.departementOfCreuse = data;
        this.departementOfCreuse = Array.of(this.departementOfCreuse);
      }
    );
  }
  communesCreuse() {
    this.communesOfCreuse = this._svcDepartement.getCommunesCreuse();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementDordogne() {
    this._svcDepartement.getDepartementDordogne().subscribe(
      data => {
        this.departementOfDordogne = data;
        this.departementOfDordogne = Array.of(this.departementOfDordogne);
      }
    );
  }
  communesDordogne() {
    this.communesOfDordogne = this._svcDepartement.getCommunesDordogne();
  }
  ////////////////////////////////////////////////////////////////////////////////////

  departementDoubs() {
    this._svcDepartement.getDepartementDoubs().subscribe(
      data => {
        this.departementOfDoubs = data;
        this.departementOfDoubs = Array.of(this.departementOfDoubs);
      }
    );
  }
  communesDoubs() {
    this.communesOfDoubs = this._svcDepartement.getCommunesDoubs();
}
//////////////////////////////////////////////////////////////////////////////////////

  departementDrome() {
    this._svcDepartement.getDepartementDrome().subscribe(
      data => {
        this.departementOfDrome = data;
        this.departementOfDrome = Array.of(this.departementOfDrome);
      }
    );
  }
  communesDrome() {
    this.communesOfDrome = this._svcDepartement.getCommunesDrome();
  }
//////////////////////////////////////////////////////////////////////////////////////

  departementEure() {
    this._svcDepartement.getDepartementEure().subscribe(
      data => {
        this.departementOfEure = data;
        this.departementOfEure = Array.of(this.departementOfEure);
      }
    );
  }
  communesEure() {
    this.communesOfEure = this._svcDepartement.getCommunesEure();
  }
//////////////////////////////////////////////////////////////////////////////////////

  departementEureEtLoir() {
    this._svcDepartement.getDepartementEureEtLoir().subscribe(
      data => {
        this.departementOfEureEtLoir = data;
        this.departementOfEureEtLoir = Array.of(this.departementOfEureEtLoir);
      }
    );
  }
  communesEureEtLoir() {
    this.communesOfEureEtLoir = this._svcDepartement.getCommunesEureEtLoir();
  }
//////////////////////////////////////////////////////////////////////////////////////

  departementFinistere() {
    this._svcDepartement.getDepartementFinistere().subscribe(
      data => {
        this.departementOfFinistere = data;
        this.departementOfFinistere = Array.of(this.departementOfFinistere);
      }
    );
  }
  communesFinistere() {
    this.communesOfFinistere = this._svcDepartement.getCommunesFinistere();
  }
//////////////////////////////////////////////////////////////////////////////////////

  departementGard() {
    this._svcDepartement.getDepartementGard().subscribe(
      data => {
        this.departementOfGard = data;
        this.departementOfGard = Array.of(this.departementOfGard);
      }
    );
  }
  communesGard() {
    this.communesOfGard = this._svcDepartement.getCommunesGard();
  }
//////////////////////////////////////////////////////////////////////////////////////

  departementHauteGaronne() {
    this._svcDepartement.getDepartementHauteGaronne().subscribe(
      data => {
        this.departementOfHauteGaronne = data;
        this.departementOfHauteGaronne = Array.of(this.departementOfHauteGaronne);
      }
    );
  }
  communesHauteGaronne() {
    this.communesOfHauteGaronne = this._svcDepartement.getCommunesHauteGaronne();
  }
//////////////////////////////////////////////////////////////////////////////////////
}
