### Angular CLI: 6.0.8
### Node: 10.0.0
### OS: linux x64
### Angular: 6.0.4

##---Installer angularCLI---
`npm install @angular/cli`


##---Updating Project Angular---
[doc : https://update.angular.io/](https://update.angular.io/)




## --- CarouselModule ---
__import CarouselModule__

>à utiliser que coté html/css

entre balise `carousel`
                 
                 `slide
                 
                    img src="assets/img/carousel01.jpg" alt=""
                
                  /slide`
             
>CarousselModule va permettre de réaliser un slider d'image très facilement.



## --- ng-bootstrap --- for HamburgerMenu

installation:
`npm install @ng-bootstrap/ng-bootstrap`

__import ngbModule__

Au niveau de la div portant la class bootstrap "collapse navbar-collapse" ajouter la directive [ngbCollapse]="isCollapsed"

Coter TS, il suffit de déclarer la variable "isCollapsed: boolean;"
et dans le constructor "this.isCollapsed = true;"

Il faut ensuite pour que les boutons marche rajouter la directive (click)= isCollapsed = !isCollapsed


>ng-bootstrap permet de faire des boutons hamburger dans le header pour la responciviter du site web.

[doc : medium.com](https://medium.com/@frypan/angular-4-collapsible-nav-bar-and-hamburger-icon-style-change-649c37cbeea5)





## --- Angular Material ---

**Dépendances** :<br>

**BrowserAnimationsModule**,<br>
**MatButtonModule**,<br>
**MatCheckboxModule**,<br>
**MatInputModule**,<br>
**MatSelectModule**,<br>
**MatSlideToggleModule**,<br>
**MatAutocompleteModule**,<br>
**MatMenuModule**,<br>



>**MatButtonModule** : Permet de faire de simples boutons !<br>
>**MatCheckboxModule** : Permet de faire des boutons checkbox!<br>
>**MatInputModule** : Permet de faire des imputs de different type!<br>
>**MatSelectModule** : Permet de faire des imputs de type select/option!<br>
>**MatSlideToggleModule** : Permet de faire des boutons de type switch!<br>
>**MatAutocompleteModule** : Permet de faire des imputs avec une autocomplession du texte!<br>
>**MatMenuModule** : Permet de faire des menus déroulant!<br>


[doc : https://v5.material.angular.io](https://v5.material.angular.io/guide/getting-started#npm)








## --- Requete Http get ---
__import httpClientModule__

Créer un model qui permet d'avoir un model de donner défini

Créer un service qui permet de retourner une observable.
Dans le service déclarer une variable de type string qui prendra l'url de l'api ultérieurement.

En paramètre du constructeur implémenter un nom de variable aux choix de type httmClient en priver.
Ensuite dans le constructeur implémenter la variable qui reçoit l'url de l'api.

Maintenant nous allons créer une fonction de type Observable qui sera un tableau du model.
Dans cette fonction, nous allons retourner notre variable en paramètre du constructeur et faire la méthode get qui seras
Le tableau du model discipline et on passe la variable de l'api en paramètre de la méthode get.

Dans le composant voulu '.ts' faire une variable de type Observable>
maintenant dans le ngOnInit (si vous vouler que sa soit a l'init du composant, si nn faire une fonction) recupérer la
variable de type observable affecter lui la variable du service (ne pas oublier de l'ajouter en parametre du constructor
en priver) avec la fonction créer dans le service..



[doc : www.youtube.com](https://www.youtube.com/watch?v=m_jBxPn-rcU&t=323s)
